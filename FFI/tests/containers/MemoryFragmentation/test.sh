#!/bin/bash

. $(dirname $(readlink --canonicalize $0))/../../common.sh

while test $# -gt 0; do
  case $1 in
    --frag-sys)
      FRAGMENT_SYSTEM=1
      shift
      ;;
    --compaction)
      COMPACTION=1
      shift
      ;;
    *)
      error "Unknown command line option!"
      exit 1
      ;;
  esac
done

# Those naive checks should be ok as we expect a "clean" system
if ! test -x tst_sys_mem-frag || ! test -x tst_mem-frag; then
  patch --strip=1 -i mem-frag-test_DEBUG.patch
  patch --strip=1 -i mem-frag-test_performance.patch
  patch --strip=1 -i mem-frag-test_totalmem.patch

  gcc -o tst_sys_mem-frag mem-frag-test.c -Wall -Werror

  image=$(build_container_image gcc)

  podman run $CONTAINER_PARAMS $image gcc -o tst_mem-frag mem-frag-test.c -Wall -Werror
  podman run $CONTAINER_PARAMS $image gcc -o tst_mmap-size mmap_size.c -DDEBUG -Wall -Werror
fi

mem_test_size() {
  size_total_mb=$(grep "MemAvailable" /proc/meminfo | awk '{ print $2 }')
  echo "$size_total_mb / (2 * 1024) * (1 - 0.1)" | bc
}

mem_size_mb=$(mem_test_size)

printf "%s\n" "Running tst_mmap-size in orderly"
podman run $CONTAINER_PARAMS --name orderly $BASE_CONTAINER_IMAGE ./tst_mmap-size $mem_size_mb > run1.log 2>&1

if test -n "$FRAGMENT_SYSTEM"; then
  printf "%s\n" "Running tst_mem-frag"
  ./tst_sys_mem-frag 15 &
  job_pid=$!
  sleep 5
else
  printf "%s\n" "Running tst_mem-frag in confusion"
  podman run $CONTAINER_PARAMS --detach --name confusion $BASE_CONTAINER_IMAGE ./tst_mem-frag 15
  poll_container_for_string confusion "buddy system fragmented" "System memory fragmented"
fi

if test -n "$COMPACTION"; then
  printf "%s\n" "Triggering compaction run"
  echo 0 > /proc/sys/vm/compact_memory
fi

sleep 1

printf "%s\n" "Running tst_mmap-size in orderly"
podman run $CONTAINER_PARAMS --name orderly $BASE_CONTAINER_IMAGE ./tst_mmap-size $mem_size_mb > run2.log 2>&1

test -n "$job_pid" && kill "$job_pid"

duration_run1=$(cat run1.log | sed '1d' | cut -d ' ' -f5)
duration_run2=$(cat run2.log | sed '1d' | cut -d ' ' -f5)

if test -z "$duration_run1" || test -z "$duration_run2"; then
  error "Failed to parse runtime duration!"
  exit 1
fi

duration_difference=$(( $duration_run1 - $duration_run2 ))
duration_difference=${duration_difference#-} # abs()
duration_rel_error=$(( $duration_run1 / 5 ))

printf "%s %d %s %d\n" "Run1: " $duration_run1 " Run2: " $duration_run2
printf "%s %d %s %d\n" "Difference: " $duration_difference " Relative error: " $duration_rel_error

if test $duration_difference -lt $duration_rel_error; then
  success "Success!"
  exit 0
else
  error "Failure!"
  exit 1
fi

