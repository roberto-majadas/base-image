# Freedom From Interference (FFI) - SHM access restrictions

This test set should investigate the isolation as well as access possibilities across container boundaries.

## Test cases

1. SHM allocated on the system is inaccessible in containers
1. SHM allocated on the system is accessible in containers when sharing IPC namespace
1. SHM allocated in one container is inaccessible by the system
1. SHM allocated in one container is accessible by the system when sharing IPC namespace
1. SHM allocated in one container is inaccessible in another container
1. SHM allocated in one container is accessible in another container when sharing IPC namespace

