#!/bin/bash

. $(dirname $(readlink --canonicalize $0))/../../common.sh

if ! test -x tst_sys_shm; then
  gcc -o tst_sys_shm -lrt shm.c -Wall -Werror
fi

if ! test -x tst_shm; then
  image=$(build_container_image gcc)
  podman run --detach $CONTAINER_PARAMS --name builder $image
  podman exec -ti builder gcc -o tst_shm -lrt shm.c -Wall -Werror
  podman rm --force builder
fi

if test -n "$FFI_QM_SCENARIO"; then
  cp ./tst_shm /var/qm/
else
  podman cp ./tst_shm $BAD_CONTAINER:/var/tst_shm
fi