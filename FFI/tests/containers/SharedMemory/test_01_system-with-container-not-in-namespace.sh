#!/bin/bash

. $(dirname $(readlink --canonicalize $0))/setup.sh

printf "%s\n" "-- Running shm"
./tst_sys_shm >/dev/null &
 
printf "%s\n" "-- Running shm test in $BAD_CONTAINER"
podman exec $BAD_CONTAINER /var/tst_shm > /dev/null