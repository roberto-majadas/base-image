#!/bin/bash

. $(dirname $(readlink --canonicalize $0))/setup.sh


printf "%s\n" "-- Running system_v_message_queues test as a server (detached mode container mode)."
podman run $CONTAINER_PARAMS -d --name confusion $BASE_CONTAINER_IMAGE ./tst_system_v_message_queues > /dev/null

printf "%s\n" "-- Running system_v_message_queues as a client in system. Expected: failure, not able to access the same queue"
./tst_sys_system_v_message_queues
