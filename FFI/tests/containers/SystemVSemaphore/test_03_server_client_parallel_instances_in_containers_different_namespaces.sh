#!/bin/bash

. $(dirname $(readlink --canonicalize $0))/setup.sh

printf "%s\n" "-- Running system_v_semaphore test in server container."
podman run $CONTAINER_PARAMS -d --name server $BASE_CONTAINER_IMAGE ./tst_system_v_semaphore > /dev/null

printf "%s\n" "-- Running system_v_semaphore test in client container. Expected: failure, not able to access the same semaphore."
podman run $CONTAINER_PARAMS --name client $BASE_CONTAINER_IMAGE ./tst_system_v_semaphore > /dev/null

