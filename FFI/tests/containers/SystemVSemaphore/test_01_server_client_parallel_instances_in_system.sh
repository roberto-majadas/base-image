#!/bin/bash

. $(dirname $(readlink --canonicalize $0))/setup.sh

printf "%s\n" "-- Running system_v_semaphore as server."
./tst_sys_system_v_semaphore >/dev/null &

printf "%s\n" "-- Running system_v_semaphore as client. Expected: success."
./tst_sys_system_v_semaphore 
