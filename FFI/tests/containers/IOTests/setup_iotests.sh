#!/bin/bash

. $(dirname $(readlink --canonicalize $0))/../../common.sh

while test $# -gt 1; do
  case $1 in
    -c|--stress-cmd)
      STRESS_CMD=$(printf "$2" $(nproc) $mem_avail)
      shift 2
      ;;
    -a|--container-opts)
      ORDERLY_OPTS="$ORDERLY_OPTS $2"
      CONFUSION_OPTS="$CONFUSION_OPTS $2"
      shift 2
      ;;
    -o|--orderly-opts)
      ORDERLY_OPTS="$ORDERLY_OPTS $2"
      shift 2
      ;;
    -c|--confusion-opts)
      CONFUSION_OPTS="$CONFUSION_OPTS $2"
      shift 2
      ;;
    --cpuset)
      CPUSET="1"
      shift
      ;;
  esac
done

if test -z "$STRESS_CMD"; then
  error "No stress command given!"
  exit 1
fi

ioping="http://ftp.halifax.rwth-aachen.de/fedora-epel/8/Everything/$(uname -m)/Packages/i/ioping-1.1-1.el8.$(uname -m).rpm"

test_image=$(build_container_image stress-ng $ioping)

