#!/bin/bash

set -euo pipefail

source /etc/os-release

# Support regex so we can use flexible matching rather than strict matching
# This helps avoid creating change detectors that are constantly blaring the
# alarm when instead we only care about a certain subset of the string matching.
if [[ "${PRETTY_NAME}" =~ "${DISTRO_NAME}" ]]; then
  echo "Success! As expected: ${PRETTY_NAME} =~ ${DISTRO_NAME}"
else
  echo "Error: the expected DISTRO_NAME was '${DISTRO_NAME}', but it was '${PRETTY_NAME}'"
  exit 1
fi
