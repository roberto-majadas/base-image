# Smoke tests - Ostree

Smoke tests for Ostree which RHIVOS image should be based on

## This Test Suite includes these tests

1. Test `ostree admin status` returns 0 meaning the image is Ostree based

2. Check `prepare-root.conf` file within initrd of the signed ostree image 

