#!/bin/bash

# Insertion of storage.conf path as a string value in to declared storage_conf parameter.
storage_conf="/etc/containers/storage.conf"

# Check if /etc/containers/storage.conf doesn't exists.
if [ ! -f $storage_conf ]; then
    echo "${storage_conf} does not exists!"
    # Exit code as no storage.conf exists on a system.
    exit 1
fi

# Insertion of "transient_store = true" as a string value in to declared transient_store parameter.
transient_store="transient_store = true"

# Check if transient_store is set to "true".
if grep -Fxq "$transient_store" "$storage_conf"; then
    # Transient store is set to true.
    echo "Transient_store is enabled."
    exit 0
else
    # Transient store is disabled or not set.
    # Insertion of "transient_store" as a string value in to declared transient parameter.
    transient="transient_store"

    # Check to what value transient_store is set.
    transient_store_value="$(grep -F "$transient" "$storage_conf")"

    # Report the Transient_store is disabled and its value and exit.
    echo "Transient_store is disabled and set to: ${transient_store_value}"
    exit 1
fi

