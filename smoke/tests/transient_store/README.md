# Smoke tests - Transient_store that ensures transient_store is enabled in storage.conf in the default RHIVOS images.

Smoke test for Transient_store.

## This Test Suite includes these tests:

1. Check that transient_store is enabled in the default RHIVOS images.
    
2. The test runs and reports that transient_store is enabled.
