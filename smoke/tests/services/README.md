# Smoke tests - Services

Smoke tests for services

## This Test Suite includes these tests

1. Check if basic services are running (systemd, NetworkManager)

2. Check podman service is not enabled on RHIVOS by default.
