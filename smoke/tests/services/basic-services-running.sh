#!/bin/bash

readarray -d ' ' -t SERVICES_ARRAY <<< "$SERVICES"
for name in "${SERVICES_ARRAY[@]}"; do
    name=${name%$'\n'}
    echo -en "$name is:\t"
    if ! systemctl is-active "$name"; then
        exit 1
    fi
done

exit 0

