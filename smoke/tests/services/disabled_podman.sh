#!/bin/bash

set -e

# The minimal image does NOT contain podman, this test should be skipped 
if [[ "${IMAGE_NAME}" == "minimal" ]]; then
   echo "Minimal image doesn't support podman."
   exit
fi

# Check podman is installed.
if ! rpm -q podman > /dev/null; then
   echo "This image has something wrong, podman isn't installed."
   exit 1 
fi

# Please note: The default RHIOVS image contains the podman package installed but the
# service is NOT enabled by default. This is the expected behaviour.
if [ $(systemctl is-enabled podman) == "disabled" ]; then
   echo "Podman service is 'not enabled' in RHIVOS image"
else
   echo "Podman service is !!ENABLED!! in RHIVOS image"
   exit 1
fi
