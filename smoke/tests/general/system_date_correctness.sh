#!/bin/bash

set -uxo pipefail

LOCAL_TIME=$(timedatectl | awk -F ': ' '/Local time/ {print $2}')
UNIVERSAL_TIME=$(timedatectl | awk -F ': ' '/Universal time/ {print $2}')

rpm -qa | grep chrony
if [ ! $? ]; then
   echo "No chrony is installed on the image"
   exit 1
fi

if [ "$(systemctl is-enabled chronyd)" != "enabled" ] || [ "$(systemctl is-active chronyd)" != "active" ]; then
   echo "Chronyd service isn't enable and active, please check it!"
   exit 1
fi

# Comparing local time with Univeral time
if [ "$(date -d "${LOCAL_TIME}" +%s)" -ne "$(date -d "${UNIVERSAL_TIME}" +%s)" ]; then
   echo "System date displays wrong, please check!"
   exit 1
fi

ERRORS=$(journalctl -u chronyd.service | grep -E 'error|fail')
if [ ! -z "$ERRORS" ]; then
   echo "There are errors or failures in chrony log!"
   echo "$ERRORS"
   exit 1
fi

echo "System date displays correctly in this image"
