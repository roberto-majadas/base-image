#!/bin/bash

set -uo pipefail

STATUS=$(sestatus | awk -F ':' '/SELinux status/ { gsub(" ",""); print $2}')
CURRENT_MODE=$(sestatus | awk -F ':' '/Current mode/ { gsub(" ",""); print $2}')
MODE_FROM_CFG=$(sestatus | awk -F ':' '/Mode from config file/ { gsub(" ",""); print $2}')

# Check selinux status
if [ ${STATUS} != "enabled" ]; then
   echo "Selinux is not enabled on this system"
   exit 1
else
   echo "Selinux is ${STATUS} on this system."

   # Compared current mode and mode from config file
   if [ ${CURRENT_MODE} == ${MODE_FROM_CFG} ]; then
      echo "Selinux current mode is the same as the mode from config file."

      if [ ${CURRENT_MODE} == "enforcing" ] && [ ${MODE_FROM_CFG} == "enforcing" ]; then
         echo "Both of the modes are Enforcing."
         exit 0
      else
         echo "Selinux current mode is: ${CURRENT_MODE}."
         echo "The mode from config file is: ${MODE_FROM_CFG}."
         echo "Either current mode or config file mode is NOT set to enforcing"
         exit 1
      fi
   else
      echo "Selinux current mode and the mode from config file are different!"
      exit 1
   fi
fi
